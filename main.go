package main

import (
	"flag"
	"fmt"
	"go/doc"
	"os"
	"path"
	"strings"

	"github.com/Masterminds/semver"
	"gitlab.com/arnottcr/rel/internal/diff"
	"gitlab.com/arnottcr/rel/internal/git"
	"gopkg.in/src-d/go-git.v4/plumbing"
)

const (
	ErrWalk = iota
	ErrMissingModule
	ErrTagFetch
	ErrTagWalk
	ErrTagRead
)

var (
	v = flag.Bool("v", false, "add v prefix to arguments")
	p = flag.String("path", "", "path to repo")
)

func main() {
	flag.Parse()
	p1 := path.Clean(*p)

	tt, err := git.Tags(p1)
	if err != nil {
		fmt.Fprintf(os.Stderr, "fetching tags: %v\n", err)
		os.Exit(ErrTagFetch)
	}

	headPkg, err := diff.HeadPackages(p1)
	if err != nil {
		fmt.Fprintf(os.Stderr, "fetching packages: %v", err)
	}

	m, err := moduleTagPrefixes(p1)
	if err != nil {
		fmt.Fprintf(os.Stderr, "walking repo for modules: %v\n", err)
		os.Exit(ErrWalk)
	}

	var headMod = map[string]map[string]*doc.Package{}
	for mpath := range m {
		headMod[mpath] = map[string]*doc.Package{}
	}

	for hpath, hpkg := range headPkg {
		var hmod string
		for mpath, _ := range m {
			if strings.HasPrefix(hpath, mpath) && len(mpath) > len(hmod) {
				hmod = mpath
			}
		}
		if _, ok := m[hmod]; !ok {
			fmt.Fprintf(os.Stderr, "package lies outside module: %s\n", hpath)
			os.Exit(ErrMissingModule)
		}
		headMod[hmod][hpath] = hpkg
	}

	var tagMod = map[string]map[string]*doc.Package{}
	var verMod = map[string]semver.Version{}
	for mpath, major := range m {
		var v semver.Version
		var t *plumbing.Reference
		tagMod[mpath] = map[string]*doc.Package{}
		err = tt.ForEach(func(r *plumbing.Reference) error {
			name := r.Name().Short()
			if !strings.HasPrefix(name, mpath) {
				return nil
			}
			rv, err := semver.NewVersion(strings.TrimPrefix(name, mpath+"v"))
			if err != nil {
				// TODO(arnottcr): convert to debug log
				fmt.Fprintf(os.Stderr, "generating tag version: %v\n", err)
				return nil
			}
			if int64(major) != rv.Major() && major == 0 && rv.Major() != 1 {
				return nil
			}
			if rv.GreaterThan(&v) {
				v, t = *rv, r
			}
			return nil
		})
		if err != nil {
			fmt.Fprintf(os.Stderr, "walking tags: %v\n", err)
			os.Exit(ErrTagWalk)
		}
		if (semver.Version{} == v) {
			verMod[mpath] = semver.Version{}
			continue
		}
		verMod[mpath] = v
		p2, err := diff.TagPackages(p1, t)
		if err != nil {
			fmt.Fprintf(os.Stderr, "reading tag packages: %v\n", err)
			os.Exit(ErrTagRead)
		}
		for k, v1 := range p2 {
			tagMod[mpath][k] = v1
		}
	}

	for mpath, d := range diff.CompareRepo(tagMod, headMod) {
		d1 := d.Difference()
		v := verMod[mpath]
		var v1 semver.Version
		switch d1 {
		case diff.Major:
			v1 = v.IncMajor()
			// TODO(arnottcr): fixup import paths
		case diff.Minor:
			v1 = v.IncMinor()
		default:
			v1 = v.IncPatch()
		}
		fmt.Printf("%s: \n  %s: v%v -> v%v\n", mpath, d1, &v, &v1)
		for ppath, d2 := range d {
			if !d2.Plus.Empty() || !d2.Minus.Empty() {
				fmt.Printf("  %s:\n", ppath) // TODO(arnottcr): grab import path out of d2
				if !d2.Plus.Empty() {
					fmt.Printf("    plus:\n")
					for _, n := range d2.Plus.Consts {
						for _, n1 := range n.Names {
							fmt.Printf("      %v\n", n1)
						}
					}
					for _, n := range d2.Plus.Vars {
						for _, n1 := range n.Names {
							fmt.Printf("      %v\n", n1)
						}
					}
					for _, n := range d2.Plus.Types {
						fmt.Printf("      %v\n", n.Name)
					}
					for _, n := range d2.Plus.Funcs {
						fmt.Printf("      %v\n", n.Name)
					}
				}
				if !d2.Minus.Empty() {
					fmt.Printf("    minus:\n")
					for _, n := range d2.Minus.Consts {
						for _, n1 := range n.Names {
							fmt.Printf("      %v\n", n1)
						}
					}
					for _, n := range d2.Minus.Vars {
						for _, n1 := range n.Names {
							fmt.Printf("      %v\n", n1)
						}
					}
					for _, n := range d2.Minus.Types {
						fmt.Printf("      %v\n", n.Name)
					}
					for _, n := range d2.Minus.Funcs {
						fmt.Printf("      %v\n", n.Name)
					}
				}
			}
		}
	}
}
