package main

import (
	"fmt"
	"io/ioutil"
	"os"
	"path"
	"path/filepath"
	"strconv"
	"strings"

	"gitlab.com/arnottcr/rel/internal/modfile"
	"gitlab.com/arnottcr/rel/internal/module"
)

func moduleTagPrefixes(dir string) (map[string]int, error) {
	var m = map[string]int{}
	return m, filepath.Walk(dir, func(p string, info os.FileInfo, err error) error {
		if !info.IsDir() && info.Name() == "go.mod" {
			v, err := moduleMajorVersion(p)
			if err != nil {
				return fmt.Errorf("fetching %s module: %v", p, err)
			}
			m[strings.TrimPrefix(path.Dir(p)+"/", dir+"/")] = v
		}
		return nil
	})
}

func moduleMajorVersion(dir string) (int, error) {
	data, err := ioutil.ReadFile(dir)
	if err != nil {
		return 0, fmt.Errorf("reading file: %v", err)
	}
	f, err := modfile.Parse("", data, nil)
	if err != nil {
		return 0, fmt.Errorf("parsing go.mod: %v", err)
	}
	_, pathMajor, ok := module.SplitPathVersion(f.Module.Mod.Path)
	if !ok {
		return 0, fmt.Errorf("parsing module version: %v", err)
	}
	if pathMajor == "" {
		return 0, nil
	}
	return strconv.Atoi(pathMajor[2:len(pathMajor)])
}
