package diff

import (
	"go/doc"
)

//go:generate stringer -type=Pill

// Difference represents the semantic difference in two apis.
type Difference int

const (
	Patch Difference = iota
	Minor
	Major
)

type Symbols struct {
	Consts []*doc.Value
	Types  []*doc.Type
	Vars   []*doc.Value
	Funcs  []*doc.Func
}

func (s Symbols) Empty() bool {
	return len(s.Consts) == 0 && len(s.Types) == 0 && len(s.Vars) == 0 && len(s.Funcs) == 0
}

func NewSymbols(p *doc.Package) Symbols {
	return Symbols{p.Consts, p.Types, p.Vars, p.Funcs}
}

type PackageDelta struct {
	Plus  Symbols
	Minus Symbols
}

func (d PackageDelta) Difference() Difference {
	if !d.Minus.Empty() {
		return Major
	}
	if !d.Plus.Empty() {
		return Minor
	}
	return Patch
}

func ComparePackage(exp, act *doc.Package) PackageDelta {
	for _, p := range []*doc.Package{exp, act} {
		if p.Name == "main" {
			exp.Consts = nil
			exp.Types = nil
			exp.Vars = nil
			exp.Funcs = nil
		}
	}

	exp.Consts, act.Consts = CompareValues(exp.Consts, act.Consts)
	for i := len(act.Types) - 1; i >= 0; i-- {
		for j := len(exp.Types) - 1; j >= 0; j-- {
			if CompareType(exp.Types[j], act.Types[i]) {
				act.Types = append(act.Types[:i], act.Types[i+1:]...)
				exp.Types = append(exp.Types[:j], exp.Types[j+1:]...)
				break
			}
		}
	}
	exp.Vars, act.Vars = CompareValues(exp.Vars, act.Vars)
	for i := len(act.Funcs) - 1; i >= 0; i-- {
		for j := len(exp.Funcs) - 1; j >= 0; j-- {
			if CompareFunc(exp.Funcs[j], act.Funcs[i]) {
				act.Funcs = append(act.Funcs[:i], act.Funcs[i+1:]...)
				exp.Funcs = append(exp.Funcs[:j], exp.Funcs[j+1:]...)
				break
			}
		}
	}
	return PackageDelta{NewSymbols(act), NewSymbols(exp)}
}

type ModuleDelta map[string]PackageDelta

func (d ModuleDelta) Difference() Difference {
	var d1 Difference
	for _, d2 := range d {
		d3 := d2.Difference()
		if d3 == Major {
			return Major
		}
		if d3 > d1 {
			d1 = d3
		}
	}
	return d1
}

func CompareModule(exp, act map[string]*doc.Package) ModuleDelta {
	var d = ModuleDelta{}
	for tpath, tpkg := range exp {
		for hpath, hpkg := range act {
			if tpath == hpath {
				delete(exp, tpath)
				delete(act, hpath)
				c := ComparePackage(tpkg, hpkg)
				if c.Difference() != Patch {
					d[tpath] = c
				}
			}
		}
	}
	for path, pkg := range exp {
		d[path] = PackageDelta{Minus: NewSymbols(pkg)}
	}
	for path, pkg := range act {
		d[path] = PackageDelta{Plus: NewSymbols(pkg)}
	}
	return d
}

type RepoDelta map[string]ModuleDelta

func CompareRepo(exp, act map[string]map[string]*doc.Package) RepoDelta {
	var d = RepoDelta{}
	for path := range exp {
		d[path] = CompareModule(exp[path], act[path])
	}
	return d
}
