module gitlab.com/arnottcr/rel

go 1.12

require (
	github.com/Masterminds/semver v1.4.2
	github.com/blang/semver v3.5.1+incompatible
	gopkg.in/src-d/go-git.v4 v4.10.0
)
