package diff

import (
	"errors"
	"fmt"
	"go/doc"
	"go/parser"
	"go/token"
	"os"
	"path"
	"path/filepath"
	"testing"
)

func packageDoc(dir string) (*doc.Package, error) {
	pkgs, err := parser.ParseDir(&token.FileSet{}, dir, nil, 0)
	if err != nil {
		return nil, fmt.Errorf("parsing pkg dir: %v", err)
	}
	if len(pkgs) != 1 {
		return nil, errors.New("invalid pkgs returned")
	}
	for _, p1 := range pkgs {
		return doc.New(p1, "", 0), nil
	}
	return nil, errors.New("absurd")
}

func TestComparePackage(t *testing.T) {
	exp, err := packageDoc("testdata/main/exp")
	if err != nil {
		t.Errorf("generating exp *doc.Package: %s", err)
	}
	act, err := packageDoc("testdata/main/act")
	if err != nil {
		t.Errorf("generating act *doc.Package: %s", err)
	}
	if ComparePackage(exp, act).Difference() != Patch {
		t.Errorf("main packages do not have apis")
	}
	pkg := "testdata/pkg/act"
	// TODO(arnottcr): consider test (pkg -> main), (main -> pkg)
	filepath.Walk(pkg, func(p string, info os.FileInfo, err error) error {
		if !info.IsDir() || p == pkg {
			return nil
		}
		var exp Difference
		pexp, err := packageDoc("testdata/pkg/exp")
		if err != nil {
			t.Errorf("generating exp *doc.Package: %s", err)
			return nil
		}
		pact, err := packageDoc(p)
		if err != nil {
			t.Errorf("generating act *doc.Package: %s", err)
			return nil
		}
		d := ComparePackage(pexp, pact)
		switch b := path.Base(p); b {
		case "new", "empty":
			exp = Major
		case "add":
			exp = Minor
		case "same", "dropunexported", "addunexported":
			exp = Patch
		default:
			t.Errorf("unregistered testdata dir: %s", b)
			return nil
		}
		if act := d.Difference(); exp != act {
			t.Errorf("expecting %v, got %v: %+v", exp, act, d)
		}
		return nil
	})
}
