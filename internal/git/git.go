package git

import (
	"fmt"

	git "gopkg.in/src-d/go-git.v4"
	"gopkg.in/src-d/go-git.v4/plumbing/storer"
)

func Tags(repo string) (storer.ReferenceIter, error) {
	r, err := git.PlainOpen(repo)
	if err != nil {
		return nil, fmt.Errorf("opening repository: %v", err)
	}
	t, err := r.Tags()
	if err != nil {
		return nil, fmt.Errorf("fetching tags: %v", err)
	}
	return t, nil
}
